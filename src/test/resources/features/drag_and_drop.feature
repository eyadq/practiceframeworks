Feature: Test Drag and Drop page

  @ui @smoke @regression @positive @dragAndDrop
  Scenario: Test functionality of the draggable squares
    Given user is on "Drag and Drop" page
    And user drags "column A" over "column B"
    And user sees the two columns "have" switched places
    And user drags "column B" over "footer"
    And user sees the two columns "have not" switched places
