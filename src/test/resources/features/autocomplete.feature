Feature: Test Autocomplete page

  @ui @smoke @regression @positive @autocomplete @autocomplete1
  Scenario: Test autocomplete option works
    Given user is on "Autocomplete" page
    And verify the log contains this <country> when user enters <partial text> and presses enter
      | partial text | country   |
      | Palest       | Palestine |
      | It           | Italy     |
    Then verify that autocompletion finds these countries for corresponding partial user text
      | partial text | country                                                        |
      | Ye           | Yemen                                                          |
      | united       | United Arab Emirates, United Kingdom, United States of America |

  @ui @smoke @regression @negative @autocomplete @autocomplete2
  Scenario: Test autocomplete option works
    Given user is on "Autocomplete" page
    Then verify that autocompletion finds these countries for corresponding partial user text
      | partial text              | country |
      | ThisIsNotARealCountryName |         |