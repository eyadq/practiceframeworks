Feature: Test Registration Form page

  @ui @smoke @regression @positive @registrationForm @registrationForm1
  Scenario: Test correctly filling out registration form
    Given user is on "Registration Form" page
    And User enters valid first name
    And User enters valid last name
    And User enters valid username
    And User enters valid email address
    And User enters valid password
    And User enters valid phone number
    And User selects gender from radio buttons
    And User enters valid date of birth
    And User selects department of employment from dropdown
    And User selects job title from dropdown
    And User selects checkbox for Java if applicable
    And user hits Sign Up button
    Then user should see success message for the registration form

  @ui @smoke @regression @negative @registrationForm @registrationForm2
  Scenario: Test incorrectly filling out registration form
    Given user is on "Registration Form" page
    And User enters a single character in the first name input field
    And User enters a second character in the first name input field
    And user hits Sign Up button
    Then user should see error message for the registration form



