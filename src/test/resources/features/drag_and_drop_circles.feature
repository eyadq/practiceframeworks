Feature: Test Drag and Drop Circles page

  @ui @smoke @regression @positive @dragAndDropCircles
  Scenario: Test functionality of the draggable squares
    Given user is on "Drag and Drop Circles" page
    And verify drop target says "Drag the small circle here."
    And user holds circle over "footer"
    And verify drop target says "Drop here."
    And verify drop target is blue
    And user holds circle over "droptarget"
    And verify drop target says "Now drop..."
    And user drags small circle to "footer"
    And verify drop target says "Try again!"
    And user drags small circle to "droptarget"
    And verify drop target says "You did great!"
    And verify drop target is blue
