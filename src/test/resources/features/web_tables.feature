Feature: When user orders logs into Order website, the user can see a list of past purchases

  @ui @smoke @regression @positive @webTables @webTables1
    Scenario: WebOrders table is keeping track of past pizza purchases
      Given user is on "Web Tables" page
      Then User searches for the names and is able to get the date of the purchase
        |Name             |Date      |
        |Alexandra Gray   |04/15/2021|
        |John Doe         |01/08/2021|
        |Stewart Dawidson |03/29/2021|
        |Bart Fisher      |01/16/2021|
        |Ned Stark        |05/12/2021|
        |Bob Martin       |12/31/2021|
        |Samuel Jackson   |12/21/2021|
        |Robert Baratheon |12/04/2021|

    @ui @db @smoke @regression @positive @webTables @webTables2
      #Note: First follow instructions from sql/practice/weborders.sql to pretend we have PracticeWebsite/DB access
      Scenario: Make sure WebTables is loading data from database
      Given user is on "Web Tables" page
      When Execute query to get all data from database
      Then verify that data matches table from UI