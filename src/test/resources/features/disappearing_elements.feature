Feature: Test Disappearing Elements page

  @ui @smoke @regression @positive @disappearingElements @disappearingElements1
  Scenario: Test Forgot Password when using a correct email
    Given user is on "Disappearing Elements" page
    And users sees four out of five potential buttons appearing
    Then verify these four buttons work as intended