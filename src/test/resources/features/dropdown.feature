Feature: Test Dropdown page

  @ui @smoke @regression @positive @dropdown @dropDown1
  Scenario: Test all available options of all dropdowns
    Given user is on "Dropdown" page
    Then User should see all options for each dropdown
    |simple|
    |birth|
    |state|
    |languages|
    |links|

  @ui @smoke @regression @positive @dropdown @dropDown2
  Scenario: Test getting and setting dates in birth dropdowns
    Given user is on "Dropdown" page
    And The date should be todays date by default
    When The user changes the year, month, date dropdowns
    Then The date should be read as a new value

  @ui @smoke @regression @positive @dropdown @dropDown3
  Scenario: Test setting options and values from state dropdown
    Given user is on "Dropdown" page
    When The the default option should be "Select a State"
    And The user picks a state such as Ohio
    Then The value of the dropdown should also be "OH" and state "Ohio"

  @ui @smoke @regression @positive @dropdown @dropDown4
  Scenario: Test setting options and values from languages dropdown
    Given user is on "Dropdown" page
    When The the default option should be all values unselected
    And The user selects all options
    Then All options should be selected in the dropdown