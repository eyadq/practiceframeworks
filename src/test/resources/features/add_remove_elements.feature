Feature: Test Add/Remove page

  @ui @smoke @regression @positive @addRemoveElements
  Scenario: Test and and delete buttons of Add/Remove page
    Given user is on "Add/Remove Elements" page
    And The user should only see one button with text of "Add Element"
    And A button with the word "Delete" should appear when the user clicks on add button
    And The delete button goes away when the user clicks on the delete button
    Then And "5" buttons with the word "Delete" should appear when the user clicks on the add button five times
