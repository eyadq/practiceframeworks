Feature: Test Context Menu page

  @ui @smoke @regression @positive @contextMenu
  Scenario: Test if javascript alert works on right click
    Given user is on "Context Menu" page
    And user right clicks on the rectangle featured on the page
    Then user should see the javascript alert