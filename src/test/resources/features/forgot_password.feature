Feature: Test Forgot Password page

  @ui @smoke @regression @positive @forgotPassword @forgotPassword1
  Scenario: Test Forgot Password when using a correct email
    Given user is on "Forgot Password" page
    And user enters a "valid" email
    And user clicks on the retrieve password button
    Then user should "not see" an error message

  @ui @smoke @regression @negative @forgotPassword @forgotPassword2
  Scenario: Test Forgot Password when using a correct email
    Given user is on "Forgot Password" page
    And user enters a "non-valid" email
    And user clicks on the retrieve password button
    Then user should "see" an error message