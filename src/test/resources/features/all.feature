Feature: Test navigating to page and test text

  @ui @smoke @regression @positive @sharedExample
  Scenario Outline: Test text of "<name of example>" page
    Given user is on "<name of example>" page
    Then verify text of "<name of example>" page is correct
    Examples:
      | name of example       |
      | A/B Testing           |
      | Add/Remove Elements   |
      | Autocomplete          |
      | Basic Auth            |
      | Checkboxes            |
      | Context Menu          |
      | Disappearing Elements |
      | Dropdown              |
      | Forgot Password       |
      | Multiple Buttons       |
      | Registration Form     |
      | Typos                 |
      | Web Tables            |