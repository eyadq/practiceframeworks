Feature: Test Checkboxes page

  @ui @smoke @regression @positive @checkboxes
  Scenario: Test checkboxes work independent of each other
    Given user is on "Checkboxes" page
    Then verify user can select checkboxes independently of each other
    And verify text of checkboxes page