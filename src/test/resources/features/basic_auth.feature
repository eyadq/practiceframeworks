Feature: Test Basic Auth page

  @ui @smoke @regression @positive @basicAuth @basicAuth1
  Scenario: Test using correct credentials on Basic Auth page
    Given user is on "Basic Auth" page
    And user logs in using correct credentials
    Then verify user has logged in by verifying text on page

  @ui @smoke @regression @negative @basicAuth @basicAuth2
  Scenario: Test using incorrect credentials on Basic Auth page
    Given user is on "Basic Auth" page
    And user logs in using incorrect credentials
    Then verify user is back on home page