Feature: Test Forgot Password page

  @ui @smoke @regression @positive @multipleButtons
  Scenario: Test Forgot Password when using a correct email
    Given user is on "Multiple Buttons" page
    And user clicks on each button and the result changes to the correct response for each button
      | buttonNumber | result                   |
      | Button 1     | Clicked on button one!   |
      | Button 2     | Clicked on button two!   |
      | Button 3     | Clicked on button three! |
      | Button 4     | Clicked on button four!  |
      | Button 5     | Clicked on button five!  |
      | Don't click!  | Now it's gone!           |
    Then verify the sixth button is now gone

