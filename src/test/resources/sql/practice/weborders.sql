/*
 Create Table
 Run the 'alter session set nls_date_format' command or the formate of the date may through the databse off
 Insert the data. The single 'INSERT ALL' command will insert all needed data.
 Commit so any test case can run successfully at any time.
 Note that PurchaseDate column is of type "date", NOT String.
 The following shows how to manipulate the date format from PurchaseDate column.
    TO_CHAR(PurchaseDate, 'MM/DD/YYYY')
 */

CREATE TABLE WebOrder(
    CustomerID int PRIMARY KEY,
    FirstName varchar(50),
    LastName varchar(50),
    PizzaType varchar(50),
    Amount int,
    PurchaseDate date,
    Street varchar(50),
    City varchar(50),
    State varchar(50),
    Zip int,
    Card varchar(50),
    CardNumber number,
    Exp varchar(5)
);

alter session set nls_date_format = 'MM/DD/YYYY';

INSERT ALL
INTO WebOrder VALUES (1, 'Alexandra', 'Gray', 'Thin Crust', 2, '04/15/2021', '7, Miller Street', 'Chicago, IL', 'US', 748, 'VISA', '321456789012', '02/24')
INTO WebOrder VALUES (2, 'John', 'Doe', 'Pepperoni', 3, '01/08/2021', '12, Cherry Ave', 'Arlington, VA', 'Canada', 76743, 'MasterCard', '980077987700', '01/23')
INTO WebOrder VALUES (3, 'Stewart', 'Dawidson', 'Sausage', 4, '03/29/2021', '19, Victor Ave', 'Cate Island', 'Canada', 24232, 'VISA', '774555444555', '03/23')
INTO WebOrder VALUES (4, 'Bart', 'Fisher', 'Cheese', 3, '01/16/2021', '35, Rock st.', 'McLean, VA', 'US', 22043, 'American Express', '444222333666', '07/26')
INTO WebOrder VALUES (5, 'Ned', 'Stark', 'Italian', 5, '05/12/2021', '17, Rose Street', 'Newcastle', 'Italy', 21444, 'MasterCard', '777888777888', '04/25')
INTO WebOrder VALUES (6, 'Bob', 'Martin', 'Cheese', 2, '12/31/2021', '22, West Ave', 'New York, NY', 'US', 11368, 'VISA', '333222111222', '06/24')
INTO WebOrder VALUES (7, 'Samuel', 'Jackson', 'Italian', 3, '12/21/2021', '13, Owl st.', 'Starberry, UT', 'US', 53665, 'MasterCard', '555743242342', '03/25')
INTO WebOrder VALUES (8, 'Robert', 'Baratheon', 'Hawaiian', 4, '12/04/2021', '29, Sanderson Ave', 'Bluecity, CA', 'US', 63325, 'MasterCard', '776565770000', '03/26')
SELECT * FROM dual;

commit;

SELECT * FROM WEBORDER;

---> Here is the answer to get the data so it matches closely from WebOrder example table
SELECT FirstName || ' ' || LastName as "Name", PizzaType as "Pizza Type", Amount as "Amount", TO_CHAR(PurchaseDate, 'MM/DD/YYYY') as "Date", Street as "Street", City as "City", State as "State", Zip as "Zip", Card as "Card", CardNumber as "Card Number", Exp as "Exp" FROM WebOrder;



