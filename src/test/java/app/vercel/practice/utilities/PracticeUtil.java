package app.vercel.practice.utilities;

import app.vercel.practice.pages.*;
import org.openqa.selenium.WebElement;

import java.util.Map;
import java.util.Objects;

public class PracticeUtil {

    public static void goToPage(String NameOfExample){
        String base = ConfigurationReader.getProperty("practice.url");
    }

    public static BasePage getInstanceOfSpecificSubClass(String exampleName){
        Map<String, BasePage> allExamples = Map.ofEntries(
                Map.entry("A/B Testing", new ABTestingPage()),
                Map.entry("Add/Remove Elements", new AddRemoveElementsPage()),
                Map.entry("Autocomplete", new AutocompletePage()),
                Map.entry("Basic Auth", new BasicAuthPage()),
                Map.entry("Checkboxes", new CheckboxesPage()),
                Map.entry("Context Menu", new ContextMenuPage()),
                Map.entry("Dropdown",  new DropdownPage()),
                Map.entry("Registration Form", new RegistrationFormPage()),
                Map.entry("Typos", new TyposPage()),
                Map.entry("Web Tables", new WebTablesPage())
        );

        return allExamples.get(exampleName);
    }

    public static String getNameForElement(WebElement element){


        String elementText = element.getText();
        String elementTagName = element.getTagName();

        if(elementText.isEmpty())
            elementText = "EmptyText";

        return "\"" + elementText + " " + elementTagName + "\"";
    }
}
