package app.vercel.practice.utilities;

import io.cucumber.java.Scenario;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class BrowserUtil {

    private static final Logger LOG = LogManager.getLogger();
    private static Duration implicitWaitTimeout;

    public static Scenario myScenario;

    public static void attachScreenShotToScenario(){
        try{
            myScenario.log("Current url is: " + Driver.getDriver().getCurrentUrl());
            final byte[] screenshot = ((TakesScreenshot) Driver.getDriver()).getScreenshotAs(OutputType.BYTES);
            myScenario.attach(screenshot, "image/png", myScenario.getName());
        } catch (WebDriverException wbd){
            wbd.getMessage();
        } catch (ClassCastException cce){
            cce.getMessage();
        }


    }

    public static void attachTextToScenario(String text){
        try{
            myScenario.attach(text, "text/plain", "text associated with step");
        } catch (WebDriverException wbd){
            wbd.getMessage();
        } catch (ClassCastException cce){
            cce.getMessage();
        }

    }

    /**
     * Performs a pause using Thread.sleep
     *
     * @param seconds
     */
    public static void sleepForSeconds(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static List<String> getTextListAsStrings(List<WebElement> elements){
        return elements.stream().map(WebElement::getText).toList();
    }





}