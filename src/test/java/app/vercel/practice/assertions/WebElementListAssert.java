package app.vercel.practice.assertions;

import org.assertj.core.api.AbstractAssert;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.IterableAssert;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class WebElementListAssert extends AbstractAssert<WebElementListAssert, List<WebElement>> {

    private List<String> listofInnerTexts = new ArrayList<>();


    protected WebElementListAssert(List<WebElement> actual) {
        super(actual, WebElementListAssert.class);

    }

    public List<WebElement> getElementList(){
        return actual;
    }

    public IterableAssert<String> getElementAsStringList(){
        return new IterableAssert<String> (actual.stream().map(WebElement::getText).toList());
    }



}
