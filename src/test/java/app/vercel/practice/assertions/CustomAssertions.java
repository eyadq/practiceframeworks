package app.vercel.practice.assertions;

import app.vercel.practice.utilities.Driver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class CustomAssertions {

    public static WebElementAssert webElement(WebElement actual){
        return new WebElementAssert(actual);
    }
    public static WebElementListAssert webElementList(List<WebElement> actual){
        return new WebElementListAssert(actual);
    }
    public static WebDriverAssert webdriver(){
        return new WebDriverAssert(Driver.getDriver());
    }
}
