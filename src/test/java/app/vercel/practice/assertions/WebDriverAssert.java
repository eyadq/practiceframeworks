package app.vercel.practice.assertions;
import app.vercel.practice.utilities.Driver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.assertj.core.api.AbstractAssert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Objects;

public class WebDriverAssert extends AbstractAssert<WebDriverAssert, WebDriver> {

    private static final Logger LOG = LogManager.getLogger();
    Duration currentImplicitWaitDuration;
    protected WebDriverAssert(WebDriver actual) {
        super(actual, WebDriverAssert.class);
    }

    public WebDriver getDriver(){
        return actual;
    }

    public WebDriverAssert assertThat(){
        return this;
    }
    /*

    Explicit Waits

     */

    public WebDriverAssert waitForAlert(int timeToWaitInSec) {
        setImplicitWaitDurationToZero();

        WebDriverWait wait = new WebDriverWait(Driver.getDriver(), Duration.ofSeconds(timeToWaitInSec));
        Alert alert = null;

        try{
            alert = wait.until(ExpectedConditions.alertIsPresent());
        } catch (TimeoutException e){
            LOG.error("Timeout while waiting for alert" + " returning a null value", e);
        }

        resetImplicitWaitDuration();

        if(alert == null)
            failWithMessage("Timeout while waiting for alert");

        return this;
    }

    public AlertAssert switchToAlert(){
        return new AlertAssert(Driver.getDriver().switchTo().alert());
    }

    private void setImplicitWaitDurationToZero(){
        currentImplicitWaitDuration = Driver.getDriver().manage().timeouts().getImplicitWaitTimeout();
        Driver.getDriver().manage().timeouts().implicitlyWait(Duration.ZERO);
    }
    private void resetImplicitWaitDuration(){
        Driver.getDriver().manage().timeouts().implicitlyWait(currentImplicitWaitDuration);
    }

}
