package app.vercel.practice.assertions;

import org.assertj.core.api.AbstractAssert;
import org.openqa.selenium.WebElement;

import java.util.List;

public class StringListAssert extends AbstractAssert<StringListAssert, List<String>> {
    protected StringListAssert(List<WebElement> webElements) {
        super(webElements.stream().map(WebElement::getText).toList(), StringListAssert.class);
    }



}
