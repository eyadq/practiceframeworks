package app.vercel.practice.assertions;

import app.vercel.practice.utilities.Driver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.assertj.core.api.AbstractAssert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;

import java.time.Duration;

public class AlertAssert extends AbstractAssert<AlertAssert, Alert> {
    private static final Logger LOG = LogManager.getLogger();
    Duration currentImplicitWaitDuration;
    protected AlertAssert(Alert actual) {
        super(Driver.getDriver().switchTo().alert(), AlertAssert.class);
    }

    public Alert getAlert(){
        return actual;
    }
    public AlertAssert hasText(){
        return this;
    }

    public AlertAssert acceptAlert(){
        actual.accept();
        return this;
    }
    public AlertAssert dismissAlert(){
        actual.dismiss();
        return this;
    }
    public AlertAssert sendKeys(String textToSend){
        actual.sendKeys(textToSend);
        return this;
    }
    public WebDriverAssert switchToDriver(){
        return new WebDriverAssert(Driver.getDriver());
    }

    public AlertAssert assertThat(){
        return this;
    }
}
