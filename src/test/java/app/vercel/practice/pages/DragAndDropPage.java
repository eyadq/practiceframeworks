package app.vercel.practice.pages;

import app.vercel.practice.utilities.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

import static app.vercel.practice.constants.DragAndDropConstants.*;

public class DragAndDropPage extends BasePage{

    public DragAndDropPage() {
        super(NAME_OF_EXAMPLE, HEADER_TEXT_EXPECTED, PARAGRAPH_TEXT_EXPECTED);
        PageFactory.initElements(Driver.getDriver(), this);
    }

    @FindBy(id = "column-a")
    public WebElement columnA;
    @FindBy(id = "column-b")
    public WebElement columnB;
    public List<WebElement> getColumnList(){
        return Driver.getDriver().findElements(By.xpath("//div[@id='columns']//div//header"));
    }

}
