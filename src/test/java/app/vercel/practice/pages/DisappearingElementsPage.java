package app.vercel.practice.pages;

import app.vercel.practice.utilities.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

import static app.vercel.practice.assertions.CustomAssertions.webElement;
import static app.vercel.practice.constants.DisappearingElementsConstants.*;

public class DisappearingElementsPage extends BasePage{

    public DisappearingElementsPage() {
        super(NAME_OF_EXAMPLE, HEADER_TEXT_EXPECTED, PARAGRAPH_TEXT_EXPECTED);
        PageFactory.initElements(Driver.getDriver(), this);
    }

    @FindBy(xpath = "//li[@class='random-item']/a")
    public List<WebElement> buttonList;

    public String getExpectedLinkForButton(String buttonText){
        buttonText = buttonText.equals("Home") ? "" :  buttonText.replace(" ", "-").toLowerCase() + "/";
        return "https://loopcamp.vercel.app/" + buttonText + "index.html";
    }

    public String getTextForButtonPageActual(String buttonText){
        WebElement header = webElement(Driver.getDriver().findElement(By.linkText(buttonText)))
                .click()
                .getElement();
        String actualText = webElement(Driver.getDriver().findElement(By.tagName("h1")))
                .waitForElementToBeVisible(5)
                .getElement().getText();
        webElement(header).waitForElementToBeVisible(5);

//        Driver.getDriver().findElement(By.linkText(buttonText)).click();
//        BrowserUtil.waitForVisibility(By.tagName("h1"), 10);
//        String actualText = Driver.getDriver().findElement(By.tagName("h1")).getText();
//        Driver.getDriver().navigate().back();
//        BrowserUtil.waitForVisibility(By.tagName("h3"), 10);
        return actualText;
    }
}
