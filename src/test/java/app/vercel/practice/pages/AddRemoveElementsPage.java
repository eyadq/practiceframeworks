package app.vercel.practice.pages;

import app.vercel.practice.utilities.BrowserUtil;
import app.vercel.practice.utilities.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

import static app.vercel.practice.constants.AddRemoveElementsConstants.*;

public class AddRemoveElementsPage extends BasePage{

    public AddRemoveElementsPage() {
        super(NAME_OF_EXAMPLE, HEADER_TEXT_EXPECTED, PARAGRAPH_TEXT_EXPECTED);
        PageFactory.initElements(Driver.getDriver(), this);
    }

    @FindBy(css = "button[class='btn btn-primary")
    public WebElement addButton;

    public List<WebElement> getDeleteButtons(){
        String xpath = "//button[.='Delete']";

        List<WebElement> listOfDeleteButtons = new ArrayList<>();

//        if(BrowserUtil.waitForVisibility(By.xpath(xpath), 0)){
//            listOfDeleteButtons = Driver.getDriver().findElement(By.id("elements")).findElements(By.xpath("//button[.='Delete']"));
//        }

        //TODO: Speed this up
        listOfDeleteButtons = Driver.getDriver().findElement(By.id("elements")).findElements(By.xpath("//button[.='Delete']"));

        return listOfDeleteButtons;

    }




}
