package app.vercel.practice.pages;

import app.vercel.practice.utilities.Driver;
import org.openqa.selenium.support.PageFactory;

import static app.vercel.practice.constants.TyposConstants.*;

public class TyposPage extends BasePage {
    public TyposPage() {
        super(NAME_OF_EXAMPLE, HEADER_TEXT_EXPECTED, PARAGRAPH_TEXT_EXPECTED);
        PageFactory.initElements(Driver.getDriver(), this);
    }
}
