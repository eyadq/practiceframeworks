package app.vercel.practice.pages;

import app.vercel.practice.utilities.Driver;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.*;

import static app.vercel.practice.constants.WebTablesConstants.*;

public class WebTablesPage extends BasePage{

    public WebTablesPage() {
        super(NAME_OF_EXAMPLE, HEADER_TEXT_EXPECTED, PARAGRAPH_TEXT_EXPECTED);
        PageFactory.initElements(Driver.getDriver(), this);
    }

    public enum PizzaTableField{
        PIZZA_TYPE(1),
        AMOUNT(2),
        DATE(3),
        STREET(4),
        CITY(5),
        STATE(6),
        ZIP(7),
        CARD(8),
        CARD_NUMBER(9),
        Exp(10);

        private int fieldOffset;

        private PizzaTableField(int fieldOffset){
            this.fieldOffset = fieldOffset;
        }

        public int getFieldOffset() {
            return fieldOffset;
        }
    }
    public String getPizzaOrderFieldUsingName(String name, PizzaTableField wantedField){
        String xpath = "//td[contains(text(), '" + name + "')]";
        WebElement nameTD = Driver.getDriver().findElement(By.xpath(xpath));

        String additionToXpath = "//following-sibling::td";

        for (int i = 0; i < wantedField.getFieldOffset(); i++) {
            xpath += additionToXpath;
        }

       return Driver.getDriver().findElement(By.xpath(xpath)).getText();
    }

    public List<String> getPizzaOrderRowUsingName(String name) {
        String xpath = "//td[contains(text(), '" + name + "')]//parent::tr//child::td";
        List<String> rowData = Driver.getDriver().findElements(By.xpath(xpath)).stream()
                .map(each-> each.getText())
                .filter(each-> !each.isEmpty() && !each.equals("Edit"))
                .toList();
        return rowData;
    }

    public List<Map<String, String>> getPizzaOrderAllRowsUsingName() {

        int amountOfRows = Driver.getDriver().findElements(By.xpath("//table[@class='SampleTable']//tbody//tr")).size();

        List<String> keys = Driver.getDriver().findElements(By.xpath("//th")).stream()
                .map(each-> each.getText())
                .filter(each-> !each.isBlank())
                .toList();

        List<Map<String, String>> listofMapsForAllRows = new LinkedList<>();
        for (int i = 0; i < amountOfRows -1; i++) {
            String xpath = "(//table[@class='SampleTable']//tbody//tr)[" + (i + 2) + "]//td";
            List<String> rowData = Driver.getDriver().findElements(By.xpath(xpath)).stream()
                    .map(each-> each.getText())
                    .filter(each-> !each.isEmpty() && !each.equals("Edit"))
                    .toList();

            Map<String, String> rowDataAsMap = new LinkedHashMap<>();
            for (int j = 0; j < keys.size(); j++) {

                rowDataAsMap.put(keys.get(j), rowData.get(j));

            }
            listofMapsForAllRows.add(rowDataAsMap);
        }

        return listofMapsForAllRows;
    }
}
