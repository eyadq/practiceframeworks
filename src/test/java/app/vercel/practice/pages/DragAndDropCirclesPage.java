package app.vercel.practice.pages;

import app.vercel.practice.utilities.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

import static app.vercel.practice.constants.DragAndDopCirclesConstants.*;

public class DragAndDropCirclesPage extends BasePage{

    public DragAndDropCirclesPage() {
        super(NAME_OF_EXAMPLE, HEADER_TEXT_EXPECTED, PARAGRAPH_TEXT_EXPECTED);
        PageFactory.initElements(Driver.getDriver(), this);
    }

    @FindBy(id = "droptarget")
    public WebElement droptarget;
    @FindBy(id = "draggable")
    public WebElement draggable;

    public String getdropTargetColor(){
        return Driver.getDriver().findElement(By.id("droptarget")).getAttribute("class");
    }

}
