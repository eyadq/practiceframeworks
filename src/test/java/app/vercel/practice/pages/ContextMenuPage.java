package app.vercel.practice.pages;

import app.vercel.practice.utilities.Driver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

import static app.vercel.practice.constants.ContextMenuConstants.*;

public class ContextMenuPage extends BasePage{
    @FindBy(css = "div[id='hot-spot']")
    public WebElement bigBox;
    @FindBy(xpath = "//div[@class='example']//following-sibling::p")
    public List<WebElement> paragraphs;

    public ContextMenuPage() {
        super(NAME_OF_EXAMPLE, HEADER_TEXT_EXPECTED, PARAGRAPH_TEXT_EXPECTED);
        PageFactory.initElements(Driver.getDriver(), this);
    }
}
