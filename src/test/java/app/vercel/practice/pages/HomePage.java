package app.vercel.practice.pages;

import app.vercel.practice.utilities.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

    public HomePage() {
        PageFactory.initElements(Driver.getDriver(), this);
    }

    public void goToPage(String NameOfPage){
        Driver.getDriver().findElement(By.linkText(NameOfPage)).click();
    }
}
