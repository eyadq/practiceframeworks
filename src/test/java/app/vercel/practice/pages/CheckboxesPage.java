package app.vercel.practice.pages;

import app.vercel.practice.utilities.Driver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

import static app.vercel.practice.constants.CheckBoxesConstants.*;

public class CheckboxesPage extends BasePage{
    @FindBy(xpath = "//input[@type='checkbox']")
    public List<WebElement> checkboxes;
    @FindBy(xpath = "//span[@class='checktext']")
    public List<WebElement> checkboxTexts;

    public CheckboxesPage() {
        super(NAME_OF_EXAMPLE, HEADER_TEXT_EXPECTED, PARAGRAPH_TEXT_EXPECTED);
        PageFactory.initElements(Driver.getDriver(), this);
    }


}
