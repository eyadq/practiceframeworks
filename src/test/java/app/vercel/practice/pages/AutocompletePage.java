package app.vercel.practice.pages;

import app.vercel.practice.utilities.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.List;

import static app.vercel.practice.constants.AutoCompleteConstants.*;

public class AutocompletePage extends BasePage{

    @FindBy(id = "myCountry")
    public WebElement input;

    @FindBy(css = "input[type='button']")
    public WebElement submitButton;
    @FindBy(id = "result")
    public WebElement log;

    public AutocompletePage() {
        super(NAME_OF_EXAMPLE, HEADER_TEXT_EXPECTED, PARAGRAPH_TEXT_EXPECTED);
        PageFactory.initElements(Driver.getDriver(), this);
    }

    public List<WebElement> getAutoCompleteList() {
        return Driver.getDriver().findElements(By.xpath("//div[@class='autocomplete-items']//child::div"));
    }
}