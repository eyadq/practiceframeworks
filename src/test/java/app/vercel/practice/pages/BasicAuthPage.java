package app.vercel.practice.pages;

import app.vercel.practice.utilities.Driver;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static app.vercel.practice.assertions.CustomAssertions.webdriver;
import static app.vercel.practice.constants.BasicAuthConstants.*;

public class BasicAuthPage extends BasePage{

    public BasicAuthPage() {
        super(NAME_OF_EXAMPLE, HEADER_TEXT_EXPECTED, PARAGRAPH_TEXT_EXPECTED);
        PageFactory.initElements(Driver.getDriver(), this);
    }

    public void login(String username, String password) {

        /*
        //Old way before AssertJ
        Alert alert = Driver.getDriver().switchTo().alert();
        alert.sendKeys(username);
        alert.accept();
        alert.sendKeys(password);
        alert.accept();
         */

        webdriver()
                .waitForAlert(5)
                .switchToAlert()
                .sendKeys(username)
                .acceptAlert()
                .sendKeys(password)
                .acceptAlert()
                .switchToDriver();
                //.waitForAlert(5); //Assert will check if alert is null and fail because we caught the TimeOutException and left the alert as null



    }
}
