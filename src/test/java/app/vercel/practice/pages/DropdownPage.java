package app.vercel.practice.pages;

import app.vercel.practice.utilities.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.time.LocalDate;
import java.util.List;

import static app.vercel.practice.constants.DropDownConstants.*;

public class DropdownPage extends BasePage {

    public Select simple;
    public Select year;
    public Select month;
    public Select day;
    public Select state;
    public Select languages;
    Select dropDownMenuLinks;
    @FindBy(tagName = "h3")
    public WebElement header;
    @FindBy(tagName = "h6")
    public List<WebElement> smallHeaders;
    @FindBy(id = "dropdown")
    private WebElement simpleWebElement;
    @FindBy(id = "year")
    private WebElement yearWebElement;
    @FindBy(id = "month")
    private WebElement monthWebElement;
    @FindBy(id = "day")
    private WebElement dayWebElement;
    @FindBy(id = "state")
    private WebElement stateWebElement;
    @FindBy(name = "Languages")
    private WebElement languagesWebElement;
    @FindBy(id = "dropdownMenuLink")
    private WebElement dropdownLinksWebElement;
    @FindBy(xpath = "//a[@class='dropdown-item']")
    public List<WebElement> listLinks;

    public DropdownPage() {
        super(NAME_OF_EXAMPLE, HEADER_TEXT_EXPECTED, PARAGRAPH_TEXT_EXPECTED);
        PageFactory.initElements(Driver.getDriver(), this);
        simple = new Select(simpleWebElement);
        year = new Select(yearWebElement);
        month = new Select(monthWebElement);
        day = new Select(dayWebElement);
        state = new Select(stateWebElement);
        languages = new Select(languagesWebElement);
    }
    @Override
    public List<String> getParagraphTextActual(){
        return Driver.getDriver().findElements(By.tagName("h6")).stream().map(WebElement::getText).filter(each -> !each.isEmpty()).toList();
    }

    public void setDate(LocalDate date) {
        year.selectByValue("" + date.getYear());
        month.selectByValue("" + (date.getMonth().getValue() - 1)); //months in java.time.Month start at 1
        day.selectByValue("" + date.getDayOfMonth());
    }

    public LocalDate getDate() {
        String yearCurrent = year.getFirstSelectedOption().getText();
        String monthCurrent = month.getFirstSelectedOption().getAttribute("value");
        String dayCurrent = day.getFirstSelectedOption().getText();

        return LocalDate.of(
                Integer.parseInt(yearCurrent),
                Integer.parseInt(monthCurrent) + 1, //months in java.time.Month start at 1
                Integer.parseInt(dayCurrent)
        );
    }
}
