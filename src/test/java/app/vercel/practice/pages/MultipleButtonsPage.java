package app.vercel.practice.pages;

import app.vercel.practice.utilities.Driver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

import static app.vercel.practice.constants.MutipleButtonsConstants.*;

public class MultipleButtonsPage extends BasePage{

    public MultipleButtonsPage() {
        super(NAME_OF_EXAMPLE, HEADER_TEXT_EXPECTED, PARAGRAPH_TEXT_EXPECTED);
        PageFactory.initElements(Driver.getDriver(), this);
    }

    @FindBy(id = "result")
    public WebElement resultText;
    @FindBy(tagName = "h4")
    public WebElement resultHeader;
    @FindBy(tagName = "button")
    public List<WebElement> buttons;
}
