package app.vercel.practice.pages;

import app.vercel.practice.utilities.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

import static app.vercel.practice.constants.ForgotPasswordConstants.*;

public class ForgotPasswordPage extends BasePage {

    public ForgotPasswordPage() {
        super(NAME_OF_EXAMPLE, HEADER_TEXT_EXPECTED, PARAGRAPH_TEXT_EXPECTED);
        PageFactory.initElements(Driver.getDriver(), this);
    }

    @FindBy(id = "email")
    public WebElement input;
    @FindBy(id = "form_submit")
    public WebElement button;
    @FindBy(className = "alert")
    public WebElement message;

    @Override
    public List<String> getParagraphTextActual(){
        return Driver.getDriver().findElements(By.tagName("label")).stream().map(WebElement::getText).toList();
    }
}
