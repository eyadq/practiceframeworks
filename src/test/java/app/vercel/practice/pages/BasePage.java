package app.vercel.practice.pages;

import app.vercel.practice.utilities.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Arrays;
import java.util.List;

public class BasePage {
    private String headerTextExpected;
    private String[] paragraphTextExpected;
    private String exampleName;
    public BasePage (String exampleName, String HeaderTextExpected, String[] ParagraphTextExpected) {
        this.exampleName = exampleName; // "Dropdown or "Context Menu", whatever matches the link text
        this.headerTextExpected = HeaderTextExpected;
        this.paragraphTextExpected = ParagraphTextExpected;
        PageFactory.initElements(Driver.getDriver(), this);
    }

    @FindBy(xpath = "//a[@target='_blank']")
    public WebElement footerLink;

    public String getHeaderTextExpected(){
        return headerTextExpected;
    }
    public String getHeaderTextActual(){
        String headerText = null;
        try{
            headerText = Driver.getDriver().findElement(By.tagName("h3")).getText();
        } catch (NoSuchElementException e){
            try {
                headerText = Driver.getDriver().findElement(By.tagName("h2")).getText();
            } catch (NoSuchElementException e2){
                headerText = "";
            }
        }
        return headerText;
    }
    public List<String> getParagraphTextExpected(){
        return Arrays.stream(paragraphTextExpected).toList();
    }
    public List<String> getParagraphTextActual(){
            return Driver.getDriver().findElements(By.xpath("//p[not(@*) or @id]")).stream().map(WebElement::getText).filter(each -> !each.isEmpty()).toList();
    }


}