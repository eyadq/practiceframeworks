package app.vercel.practice.pages;

import app.vercel.practice.utilities.Driver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import static app.vercel.practice.constants.RegistrationFormConstants.*;

public class RegistrationFormPage extends BasePage{

    public RegistrationFormPage(){
        super(NAME_OF_EXAMPLE, HEADER_TEXT_EXPECTED, PARAGRAPH_TEXT_EXPECTED);
        PageFactory.initElements(Driver.getDriver(), this);
        department = new Select(departmentWebElement);
        jobTitle = new Select(jobTitleWebElement);
    }

    public Select department;
    public Select jobTitle;
    @FindBy(name = "firstname")
    public WebElement firstNameInput;
    @FindBy(name = "lastname")
    public WebElement lastNameInput;
    @FindBy(name = "username")
    public WebElement usernameInput;
    @FindBy(name = "email")
    public WebElement emailInput;
    @FindBy(name = "password")
    public WebElement passwordInput;
    @FindBy(name = "phone")
    public WebElement phoneInput;
    @FindBy(name = "birthday")
    public WebElement birthdayInput;
    @FindBy(name = "department")
    private WebElement departmentWebElement;
    @FindBy(name = "job_title")
    private WebElement jobTitleWebElement;
    @FindBy(id = "inlineCheckbox1")
    public WebElement checkboxCPlusPlus;
    @FindBy(id = "inlineCheckbox2")
    public WebElement checkboxJava;
    @FindBy(id = "inlineCheckbox3")
    public WebElement checkboxJavaScript;
    @FindBy(id = "wooden_spoon")
    public WebElement signUpButton;
    @FindBy(xpath = "//input[@value='male']")
    public WebElement maleRadioButton;
    @FindBy(xpath = "//input[@value='female']")
    public WebElement femaleRadioButton;
    @FindBy(xpath = "//input[@value='other']")
    public WebElement otherRadioButton;
    @FindBy(className = "alert")
    public WebElement successMessage;
}
