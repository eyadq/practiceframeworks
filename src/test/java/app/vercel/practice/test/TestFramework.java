package app.vercel.practice.test;

import app.vercel.practice.constants.HomeConstants;
import app.vercel.practice.pages.AddRemoveElementsPage;
import app.vercel.practice.pages.HomePage;
import app.vercel.practice.utilities.ConfigurationReader;
import app.vercel.practice.utilities.DB_Utility;
import app.vercel.practice.utilities.Driver;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebElement;

import java.util.Arrays;
import java.util.List;

import static app.vercel.practice.assertions.CustomAssertions.webElement;
import static app.vercel.practice.assertions.CustomAssertions.webElementList;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


public class TestFramework {
    public static class TestFrameworks {
        public static void main(String[] args) {
            System.out.println("Hello world!");
        }

        @DisplayName("Testing if Driver navigates to correct webpage")
        @Test
        public void testDriver(){
            Driver.getDriver().get(ConfigurationReader.getProperty("practice.url"));
            Assertions.assertEquals(ConfigurationReader.getProperty("practice.url"), Driver.getDriver().getCurrentUrl(),
                    "The driver did not find the correct URL. Very simple test failed. Check framework.");
        }

        @DisplayName("Test DB Connection and ConfigReader")
        @Test
        public void testDbAndCR(){

            String url = ConfigurationReader.getProperty("db.url");
            String username = ConfigurationReader.getProperty("db.username");
            String password = ConfigurationReader.getProperty("db.password");

            DB_Utility.createConnection(url, username, password);
            DB_Utility.runQuery("SELECT COUNT(*) FROM EMPLOYEES WHERE FIRST_NAME = 'John'");

            // Get me how many employees we have with First name "John"
            String actualNumber = DB_Utility.getCellValue(1,1); // 3
            String expectedNumber = 3 + "";

            Assertions.assertEquals(expectedNumber, actualNumber);
            DB_Utility.destroy();

        }

        @Test
        public void testCustomAssertions(){
            Driver.getDriver().get("https://loopcamp.vercel.app/add-remove-elements.html");
            AddRemoveElementsPage addRemoveElementsPage = new AddRemoveElementsPage();
            WebElement addElement = webElement(addRemoveElementsPage.addButton)
                                            .assertThat().hasText("Add Element")
                                            .waitForElementToBeClickable(5)
                                            .getElement();
            addElement.click();
            webElement(addRemoveElementsPage.footerLink).hasLink(HomeConstants.MAIN_FOOTER_LINK);
        }

        @Test
        public void testAddDeletePage(){
            Driver.getDriver().get(ConfigurationReader.getProperty("practice.url"));
            new HomePage().goToPage("Add/Remove Elements");

            AddRemoveElementsPage addRemoveElementsPage = new AddRemoveElementsPage();
            webElement(addRemoveElementsPage.addButton)
                    .click()
                    .click()
                    .click()
                    .click();

            int numberOfDeleteButtonsActuallyByDefault = addRemoveElementsPage.getDeleteButtons().size();
            int numberOfDeleteButtonsExpected = 4;
            assertThat(numberOfDeleteButtonsActuallyByDefault).isEqualTo(numberOfDeleteButtonsExpected)
                    .as("Number of delete buttons by default should be " + numberOfDeleteButtonsActuallyByDefault + " not: " + numberOfDeleteButtonsExpected);


            //Will fail as List contains 4 delete buttons, not 3
            webElementList(addRemoveElementsPage.getDeleteButtons())
                    .getElementAsStringList()
                    .hasSize(4)
                    .isEqualTo(Arrays.asList("Delete", "Delete", "Delete"))
                    .as("Number of delete buttons by default should be " + numberOfDeleteButtonsActuallyByDefault + " not: " + numberOfDeleteButtonsExpected);

        }
    }
}
