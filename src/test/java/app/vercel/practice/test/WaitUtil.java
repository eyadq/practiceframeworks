package app.vercel.practice.test;

import app.vercel.practice.utilities.BrowserUtil;
import app.vercel.practice.utilities.Driver;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class WaitUtil {

    @Test
    public void test(){
        Driver.getDriver().get("https://www.google.com");
        WebElement element = Driver.getDriver().findElement(By.name("q"));
        WebElement one = waitForElement(10, ExpectedConditions.elementToBeClickable(element));
        WebElement two = waitForElement(ExpectedConditions.elementToBeClickable(element));

        one.sendKeys("hello");
        two.clear();

    }

    public static WebElement waitForElement(int timeInSec, ExpectedCondition expectedCondition){
        Duration duration = Driver.getDriver().manage().timeouts().getImplicitWaitTimeout();
        Driver.getDriver().manage().timeouts().implicitlyWait(Duration.ZERO);
        WebDriverWait wait = new WebDriverWait(Driver.getDriver(), Duration.ofSeconds(timeInSec));
        Driver.getDriver().manage().timeouts().implicitlyWait(duration);
        return (WebElement) wait.until(expectedCondition);
    }

    public static WebElement waitForElement(ExpectedCondition expectedCondition){
        Duration duration = Driver.getDriver().manage().timeouts().getImplicitWaitTimeout();
        Driver.getDriver().manage().timeouts().implicitlyWait(Duration.ZERO);
        WebDriverWait wait = new WebDriverWait(Driver.getDriver(), Duration.ofSeconds(5));
        Driver.getDriver().manage().timeouts().implicitlyWait(duration);
        return (WebElement) wait.until(expectedCondition);
    }
}
