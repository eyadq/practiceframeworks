package app.vercel.practice.steps;

import app.vercel.practice.enums.States;
import app.vercel.practice.pages.DropdownPage;
import app.vercel.practice.utilities.*;

import static app.vercel.practice.constants.DropDownConstants.*;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import org.openqa.selenium.WebElement;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.List;

public class DropdownStepDef {

    DropdownPage dropdownPage = new DropdownPage();

    /*
    -----------------------------------
    User clicks on dropdown and sees all available options options
    -----------------------------------
     */

    @Then("User should see all options for each dropdown")
    public void userShouldSeeAllOptionsForEachDropdown(List<String> dropdowns) {
        for (String dropdown : dropdowns) {

            switch (dropdown){
                case "simple":
                    String[] simpleOptionsActual = dropdownPage.simple.getOptions().stream().map(WebElement::getText).toArray(String[]::new);
                    assertThat(simpleOptionsActual).isEqualTo(SIMPLE_OPTIONS)
                            .as("All options for simple dropdown");
                    break;
                case "birth":
                    int[] yearOptionsActual = dropdownPage.year.getOptions().stream().map(WebElement::getText).mapToInt(Integer::parseInt).toArray();
                    int[] yearOptionsExpected = ArrayUtil.getIntArray(LocalDate.now().getYear()-99, LocalDate.now().getYear());
                    assertThat(yearOptionsActual).isEqualTo(yearOptionsExpected)
                            .as("All options for year dropdown");

                    String[] monthOptionsActual = dropdownPage.month.getOptions().stream().map(WebElement::getText).toArray(String[]::new);
                    String[] monthOptionsExpected = Arrays.stream(Month.values()).map(each-> StringUtil.toNameCase(each.toString())).toArray(String[]::new);
                    assertThat(monthOptionsActual).isEqualTo(monthOptionsExpected)
                            .as("All options for month dropdown");

                    int dayAmountOfOptionsActual = dropdownPage.day.getOptions().size();
                    int dayAmountOfOptionsExpected = Month.of(Integer.parseInt(dropdownPage.month.getFirstSelectedOption().getAttribute("value")) + 1).length(DateUtil.isLeapYear());
                    assertThat(dayAmountOfOptionsActual).isEqualTo(dayAmountOfOptionsExpected)
                            .as("All options for day dropdown");
                    break;
                case "state":
                    States[] states = States.values();
                    for (int i = 1; i< dropdownPage.state.getOptions().size(); i++){ //index 0 has value of "" and text of "Select a State
                        WebElement element = dropdownPage.state.getOptions().get(i);

                        String stateNameActual = element.getText();
                        String stateNameExpected = states[i-1].getStateName();
                        assertThat(stateNameActual).as("State name of dropdown option").isEqualTo(stateNameExpected)
                                .as("All options for state dropdown");

                        String stateValueActual = element.getAttribute("value");
                        String stateValueExpected = states[i-1].toString();
                        assertThat(stateValueActual).as("State value of dropdown option").isEqualTo(stateValueExpected)
                                .as("All values for state dropdown");
                    }
                    break;
                case "languages":
                    List<WebElement> options = dropdownPage.languages.getOptions();
                    for (int i = 0; i < options.size(); i++) {
                        assertThat(options.get(i).getText()).isEqualTo(LANGUAGES_OPTIONS[i])
                                .as("Options in languages drop down " + i);
                        assertThat(options.get(i).getAttribute("value")).isEqualTo(LANGUAGES_VALUES[i])
                                .as("Values in languages drop down " + i);
                        options.get(i).click();
                    }
                    break;
                case "links":
                    for (WebElement linkWebElement : dropdownPage.listLinks){
                        String urlLink = linkWebElement.getAttribute("href");
                        boolean isValid = FileUtil.verifyUrl(urlLink);
                        assertThat(isValid).isTrue()
                                .as("Invalid URL in links dropdown: " + urlLink);
                    }
            }

        }
    }

    /*
    -----------------------------------
    Scenario: User can change date from date dropdowns
    -----------------------------------
     */

    @Given("The date should be todays date by default")
    public void the_date_should_be_todays_date_by_default() {
        assertThat(dropdownPage.getDate()).isEqualTo(LocalDate.now()).as("Default value of date dropdowns is not set to today's date");
        BrowserUtil.attachScreenShotToScenario();
    }

    LocalDate randomDate = DateUtil.getRandomDate();
    @When("The user changes the year, month, date dropdowns")
    public void the_user_changes_the_year_month_date_dropdowns() {
        dropdownPage.setDate(randomDate);
        BrowserUtil.attachScreenShotToScenario();
    }

    @Then("The date should be read as a new value")
    public void the_date_should_be_read_as_a_new_value() {
        assertThat(randomDate).isEqualTo(dropdownPage.getDate())
                .as("Date value of date dropdowns after setting new date is not set to that date");
    }

    /*
    -----------------------------------
    Scenario: User can choose value from state dropdown
    -----------------------------------
     */

    @When("The the default option should be {string}")
    public void the_the_default_option_should_be(String string) {
        assertThat(dropdownPage.state.getFirstSelectedOption().getAttribute("value")).isEqualTo(STATE_DEFAULT_VALUE)
                .as("Default option for state");
        assertThat(dropdownPage.state.getFirstSelectedOption().getText()).isEqualTo(STATE_DEFAULT_TEXT)
                .as("Default text for state");
        BrowserUtil.attachScreenShotToScenario();
    }

    @When("The user picks a state such as Ohio")
    public void the_user_picks_a_state_such_as_ohio() {
        dropdownPage.state.selectByValue(States.OH.name());
        BrowserUtil.attachScreenShotToScenario();
    }

    @Then("The value of the dropdown should also be {string} and state {string}")
    public void the_value_of_the_dropdown_should_also_be_and_state(String value, String stateName) {
        assertThat(dropdownPage.state.getFirstSelectedOption().getText()).isEqualTo(States.OH.getStateName())
                .as("Option after changing should be Ohio");
    }

    /*
    -----------------------------------
    Scenario: User can choose any amount of values from languages dropdown
    -----------------------------------
     */

    @When("The the default option should be all values unselected")
    public void the_the_default_option_should_be_all_values_unselected() {
        dropdownPage.languages.getOptions().forEach(each -> assertThat(each.isSelected()).isFalse()
                .as("Default state for languages option should be false") );
        BrowserUtil.attachScreenShotToScenario();
    }

    @When("The user selects all options")
    public void the_user_selects_all_options() {
        dropdownPage.languages.getOptions().forEach(WebElement::click);
        BrowserUtil.attachScreenShotToScenario();
    }
    @Then("All options should be selected in the dropdown")
    public void all_options_should_be_selected_in_the_dropdown() {
        dropdownPage.languages.getOptions().forEach(each -> assertThat(each.isSelected()).isTrue()
                .as("New state for languages option should be true") );

    }


}