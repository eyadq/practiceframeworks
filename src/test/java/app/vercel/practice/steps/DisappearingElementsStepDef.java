package app.vercel.practice.steps;

import app.vercel.practice.constants.DisappearingElementsConstants;
import app.vercel.practice.pages.DisappearingElementsPage;
import app.vercel.practice.utilities.BrowserUtil;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebElement;

import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import static app.vercel.practice.assertions.CustomAssertions.webElement;

public class DisappearingElementsStepDef {

    DisappearingElementsPage disappearingElementsPage = new DisappearingElementsPage();

    @Given("users sees four out of five potential buttons appearing")
    public void users_sees_four_out_of_five_potential_buttons_appearing() {
        List<String> buttons = BrowserUtil.getTextListAsStrings(disappearingElementsPage.buttonList);
        assertThat(buttons).hasSize(4)
                .as("The amount of buttons by design is 4 buttons loading, not: " + buttons.size());
        assertThat(buttons).isSubsetOf(DisappearingElementsConstants.ALL_POTENTIAL_BUTTONS)
                .as("The buttons as a collection should have been a subset of <%s>", (Object) DisappearingElementsConstants.ALL_POTENTIAL_BUTTONS);
    }

    @Then("verify these four buttons work as intended")
    public void verify_these_four_buttons_work_as_intended() {

        for (WebElement button : disappearingElementsPage.buttonList) {
            String buttonTextActual = button.getText();
            String buttonLinkExpected = disappearingElementsPage.getExpectedLinkForButton(buttonTextActual);
            webElement(button).hasLink(buttonLinkExpected);
        }
    }
}
