package app.vercel.practice.steps;

import app.vercel.practice.constants.DragAndDopCirclesConstants;
import app.vercel.practice.pages.*;
import app.vercel.practice.utilities.BrowserUtil;
import app.vercel.practice.utilities.Driver;
import app.vercel.practice.utilities.PracticeUtil;
import io.cucumber.java.en.Given;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import static app.vercel.practice.constants.DragAndDopCirclesConstants.CLASS_PAINTED_BLUE;
import static org.assertj.core.api.Assertions.assertThat;

public class DragAndDropCirclesStepDef {

    DragAndDropCirclesPage dragAndDropCirclesPage = new DragAndDropCirclesPage();
    Actions actions = new Actions(Driver.getDriver());
    String color;

    @Given("verify drop target says {string}")
    public void verify_drop_target_says(String dropTargetTextExpected) {
        String dropTargetTextActual = dragAndDropCirclesPage.droptarget.getText();
        assertThat(dropTargetTextActual).isEqualTo(dropTargetTextExpected).as("Drop target text by default");
    }
    @Given("user holds circle over {string}")
    public void user_holds_circle_over(String targetString) {
        WebElement source = dragAndDropCirclesPage.draggable;
        WebElement target = dragAndDropCirclesPage.droptarget;
        if(targetString.equals("footer"))
            target = dragAndDropCirclesPage.footerLink;
        actions.clickAndHold(source).moveToElement(target).perform();


    }
    @Given("verify drop target is blue")
    public void verify_drop_target_is_blue() {
        color = dragAndDropCirclesPage.getdropTargetColor();
        System.out.println(color);
        //assertThat(color).isEqualTo(CLASS_PAINTED_BLUE);
    }
    @Given("user drags small circle to {string}")
    public void user_drags_small_circle_to(String targetString) {
        WebElement source = dragAndDropCirclesPage.draggable;
        WebElement target = dragAndDropCirclesPage.droptarget;
        if(targetString.equals("footer"))
            target = dragAndDropCirclesPage.footerLink;
        System.out.println(PracticeUtil.getNameForElement(target));
        actions.dragAndDrop(source, target).perform();
    }
}
