package app.vercel.practice.steps;

import app.vercel.practice.pages.ForgotPasswordPage;
import app.vercel.practice.utilities.BrowserUtil;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.By;

import static app.vercel.practice.constants.ForgotPasswordConstants.EMAIL_INVALID;
import static app.vercel.practice.constants.ForgotPasswordConstants.EMAIL_VALID;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class ForgotPasswordStepDef {

    ForgotPasswordPage forgotPasswordPage = new ForgotPasswordPage();

    @Given("user enters a {string} email")
    public void user_enters_a_email(String valid) {
        String email = (valid.equals("valid")) ? EMAIL_VALID : EMAIL_INVALID;
        forgotPasswordPage.input.sendKeys(email);
    }
    @Given("user clicks on the retrieve password button")
    public void user_clicks_on_the_retrieve_password_button() {
        forgotPasswordPage.button.click();
    }
    @Then("user should {string} an error message")
    public void user_should_an_error_message(String seesMessage) {
        //TODO: reimplement this
//        boolean isMessageVisibleActual = BrowserUtil.waitForVisibility(By.className("alert"), 2);
//        boolean isMessageVisibleExpected = seesMessage.equals("see");
//        assertThat(isMessageVisibleActual).isEqualTo(isMessageVisibleExpected);
    }
}
