package app.vercel.practice.steps;

import app.vercel.practice.pages.MultipleButtonsPage;
import app.vercel.practice.utilities.BrowserUtil;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.assertj.core.api.AssertionsForClassTypes;
import org.openqa.selenium.WebElement;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


import static org.assertj.core.api.Assertions.*;




public class MutipleButtonsStepDef {
    private MultipleButtonsPage multipleButtonsPage = new MultipleButtonsPage();
    private Set<String> keys;

    @Given("user clicks on each button and the result changes to the correct response for each button")
    public void user_clicks_on_each_button_and_the_result_changes_to_the_correct_response_for_each_button(Map<String, String> map) {
        keys = new HashSet<>(map.keySet());
        keys.removeIf(each-> each.equals("buttonNumber") || each.equals("Don't click!"));
        for (WebElement button : multipleButtonsPage.buttons) {
            String resultTextExpected = map.get(button.getText());
            button.click();
            assertThat(multipleButtonsPage.resultText.getText()).isEqualTo(resultTextExpected);
        }
    }
    @Then("verify the sixth button is now gone")
    public void verify_the_sixth_button_is_now_gone() {
        List<String> buttons = BrowserUtil.getTextListAsStrings(multipleButtonsPage.buttons);
        assertThat(buttons).isSubsetOf(keys).doesNotContain("Don't click!");
    }
}
