package app.vercel.practice.steps;

import app.vercel.practice.pages.WebTablesPage;
import app.vercel.practice.utilities.DB_Utility;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;

import java.util.List;
import java.util.Map;

import static app.vercel.practice.constants.WebTablesConstants.*;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class WebTablesStepDef {

    /*
       -----------------------------------
       Scenario: Fill in the registration form
       -----------------------------------
        */

    @Then("User searches for the names and is able to get the date of the purchase")
    public void user_searches_for_the_names_and_is_able_to_get_the_date_of_the_purchase(List<Map<String, String> > expectedInfoList) {
        WebTablesPage webTablesPage = new WebTablesPage();
        for (Map<String, String> expectedInfo : expectedInfoList){
            String actualDate = webTablesPage.getPizzaOrderFieldUsingName(expectedInfo.get("Name"), WebTablesPage.PizzaTableField.DATE);
            assertThat(actualDate).isEqualTo(expectedInfo.get("Date"))
                    .as("Actual date from table did not match expected date from feature file");
        }

    }

     /*
    -----------------------------------
    Scenario: Make sure WebTables is loading data from database
    -----------------------------------
     */

    List<Map<String, String>> allRowsFromDatabase;
    @When("Execute query to get all data from database")
    public void execute_query_to_get_all_data_from_database() {
        DB_Utility.runQuery(GET_ALL_DATA);
        allRowsFromDatabase = DB_Utility.getAllRowAsListOfMap();

    }
    @Then("verify that data matches table from UI")
    public void verify_that_data_matches_table_from_ui() {
        WebTablesPage webTablesPage = new WebTablesPage();
        assertThat(webTablesPage.getPizzaOrderAllRowsUsingName()).isEqualTo(allRowsFromDatabase)
                .as("Actual date from table did not match expected date from feature file");
    }
}
