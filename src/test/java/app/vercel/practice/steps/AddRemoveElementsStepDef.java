package app.vercel.practice.steps;

import app.vercel.practice.pages.AddRemoveElementsPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

import static app.vercel.practice.constants.AddRemoveElementsConstants.HEADER_TEXT_EXPECTED;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class AddRemoveElementsStepDef {

    AddRemoveElementsPage addRemoveElementsPage = new AddRemoveElementsPage();

    @Given("The user should only see one button with text of {string}")
    public void the_user_should_only_see_one_button_with_text_of(String textOfAddButtonExpected) {

        int numberOfDeleteButtonsActuallyByDefault = addRemoveElementsPage.getDeleteButtons().size();
        int numberOfDeleteButtonsExpected = 0;
        assertThat(numberOfDeleteButtonsActuallyByDefault).isEqualTo(numberOfDeleteButtonsExpected)
                .as("Number of delete buttons by default should be " + numberOfDeleteButtonsActuallyByDefault + " not: " + numberOfDeleteButtonsExpected);

        String addButtonTextActual = addRemoveElementsPage.addButton.getText();
        assertThat(addButtonTextActual).isEqualTo(textOfAddButtonExpected)
                .as("The text of the add button should be \"" + textOfAddButtonExpected + "\" and not: " + addButtonTextActual);
    }
    @Given("A button with the word {string} should appear when the user clicks on add button")
    public void a_button_with_the_word_should_appear_when_the_user_clicks_on_add_button(String textOfDeleteButtonExpected) {
        addRemoveElementsPage.addButton.click();
        int numberOfDeleteButtonsActual = addRemoveElementsPage.getDeleteButtons().size();
        int numberOfDeleteButtonsExpected = 1;
        assertThat(numberOfDeleteButtonsActual).isEqualTo(numberOfDeleteButtonsExpected)
                .as("Number of delete buttons by default after clicking add once should be " + numberOfDeleteButtonsActual + " not: " + numberOfDeleteButtonsExpected);;

        String textOfDeleteButtonActual = addRemoveElementsPage.getDeleteButtons().get(0).getText();
        assertThat(textOfDeleteButtonActual).isEqualTo(textOfDeleteButtonExpected)
                .as("The text of the delete button should be \"" + textOfDeleteButtonExpected + "\" and not: " + textOfDeleteButtonActual);

    }
    @Given("The delete button goes away when the user clicks on the delete button")
    public void the_delete_button_goes_away_when_the_user_clicks_on_the_delete_button() {
        addRemoveElementsPage.getDeleteButtons().get(0).click();
        int numberOfDeleteButtonsActual = addRemoveElementsPage.getDeleteButtons().size();
        int numberOfDeleteButtonsExpected = 0;
        assertThat(numberOfDeleteButtonsActual).isEqualTo(numberOfDeleteButtonsExpected)
                .as("Number of delete buttons by default after clicking on only delete button should be" + numberOfDeleteButtonsActual + " not: " + numberOfDeleteButtonsExpected);;
    }
    @Given("And {string} buttons with the word {string} should appear when the user clicks on the add button five times")
    public void and_buttons_with_the_word_should_appear_when_the_user_clicks_on_the_add_button_five_times(String numberOfButtons, String textOfDeleteButtonExpected) {
        int numberOfDeleteButtonsExpected = Integer.parseInt(numberOfButtons);
        for (int i = 1; i <= numberOfDeleteButtonsExpected; i++) {
            addRemoveElementsPage.addButton.click();
        }
        int numberOfDeleteButtonsActual = addRemoveElementsPage.getDeleteButtons().size();
        assertThat(numberOfDeleteButtonsActual).isEqualTo(numberOfDeleteButtonsExpected)
                .as("Number of delete buttons after clicking add button " + numberOfDeleteButtonsExpected + " times should be " + numberOfDeleteButtonsActual + " not: " + numberOfDeleteButtonsExpected);
    }
}
