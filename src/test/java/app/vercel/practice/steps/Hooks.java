package app.vercel.practice.steps;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import app.vercel.practice.utilities.DB_Utility;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import app.vercel.practice.utilities.ConfigurationReader;
import app.vercel.practice.utilities.Driver;
import app.vercel.practice.utilities.BrowserUtil;

import java.time.Duration;

public class Hooks {

    Logger LOG = LogManager.getLogger();

    @Before("@ui")
    public void setUpUI(Scenario scenario){
        Driver.getDriver();
        Driver.getDriver().manage().timeouts().implicitlyWait(Duration.ZERO);
        Driver.getDriver().manage().window().maximize();
        BrowserUtil.myScenario = scenario;
    }


    @After("@ui")
    public void tearDownUI(Scenario scenario){
        if(scenario.isFailed()){
            final byte[] screenshot = ((TakesScreenshot) Driver.getDriver()).getScreenshotAs(OutputType.BYTES);
            scenario.attach(screenshot,"image/png", scenario.getName());
        }
        //Driver.closeDriver();
    }

    @Before("@db")
    public void SetUpDb(){
        String url = ConfigurationReader.getProperty("db.url");
        String username = ConfigurationReader.getProperty("db.username");
        String password = ConfigurationReader.getProperty("db.password");
        DB_Utility.createConnection(url, username, password);
    }

    @After("@db")
    public void tearDownDb(){
        DB_Utility.destroy();
    }

    @Before
    public void setUpLOG(){
        LOG.info("..................Started Automation..................");
    }
    @After
    public void tearDownLOG(){
        LOG.info("...................Ended Automation...................");
    }
}
