package app.vercel.practice.steps;

import app.vercel.practice.pages.CheckboxesPage;
import app.vercel.practice.utilities.Driver;
import io.cucumber.java.en.Then;
import org.openqa.selenium.interactions.Actions;

import static app.vercel.practice.constants.CheckBoxesConstants.*;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class CheckboxesStepDef {

    CheckboxesPage checkboxesPage = new CheckboxesPage();

    @Then("verify user can select checkboxes independently of each other")
    public void verify_user_can_select_checkboxes_independently_of_each_other() {
        assertThat(checkboxesPage.checkboxes.get(0).isSelected()).isFalse().as("checkbox 1 selected by default");
        assertThat(checkboxesPage.checkboxes.get(1).isSelected()).isTrue().as( "checkbox 2 selected by default");


        checkboxesPage.checkboxes.get(0).click();
        assertThat(checkboxesPage.checkboxes.get(0).isSelected()).isTrue().as("checkbox 1 is selected after emulated mouse click");
        assertThat(checkboxesPage.checkboxes.get(1).isSelected()).isTrue().as( "checkbox 2 is still true after selecting checkbox 1");

        new Actions(Driver.getDriver()).moveToElement(checkboxesPage.checkboxes.get(1)).click().perform();
        assertThat(checkboxesPage.checkboxes.get(1).isSelected()).isFalse().as("checkbox 2 is deselected after emulated mouse click");
        assertThat(checkboxesPage.checkboxes.get(0).isSelected()).isTrue().as("checkbox 1 is still selected after deselecting checkbox 2");
    }
    @Then("verify text of checkboxes page")
    public void verify_text_of_checkboxes_page() {
        for (int i = 0; i < checkboxesPage.checkboxTexts.size(); i++)
            assertThat(checkboxesPage.checkboxTexts.get(i).getText()).isEqualTo(CHECKBOX_LABELS[i]).as("Checkbox label " + i);

        for (int i = 0; i < checkboxesPage.checkboxes.size(); i++)
            assertThat(checkboxesPage.checkboxes.get(i).getAttribute("name")).isEqualTo(CHECKBOX_OPTIONS[i]).as("checkbox options for " + i);
    }
}
