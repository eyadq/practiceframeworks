package app.vercel.practice.steps;

import app.vercel.practice.pages.DragAndDropPage;
import app.vercel.practice.utilities.Driver;
import io.cucumber.java.en.Given;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

import static app.vercel.practice.assertions.CustomAssertions.webElement;
import static app.vercel.practice.constants.DragAndDropConstants.BOX_A_TEXT;
import static app.vercel.practice.constants.DragAndDropConstants.BOX_B_TEXT;


public class DragAndDropStepDef {
    DragAndDropPage dragAndDropPage = new DragAndDropPage();
    Actions actions = new Actions(Driver.getDriver());

    @Given("user drags {string} over {string}")
    public void user_drags_over(String left, String right) {
        WebElement source = dragAndDropPage.getColumnList().get(0);
        WebElement target = dragAndDropPage.getColumnList().get(1);
        if(right.equals("footer"))
            target = dragAndDropPage.footerLink;
        actions.dragAndDrop(source, target).perform();
    }
    @Given("user sees the two columns {string} switched places")
    public void user_sees_the_two_columns_switched_places(String condition) {
        List<WebElement> columns = dragAndDropPage.getColumnList();
        WebElement columnLeft = columns.get(0);
        WebElement columnRight = columns.get(1);
        webElement(columnLeft).hasText(BOX_B_TEXT);
        webElement(columnRight).hasText(BOX_A_TEXT);
    }
}
