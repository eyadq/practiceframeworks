package app.vercel.practice.steps;

import app.vercel.practice.pages.BasicAuthPage;
import app.vercel.practice.utilities.BrowserUtil;
import app.vercel.practice.utilities.Driver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.datafaker.Faker;


import static app.vercel.practice.constants.BasicAuthConstants.*;
import static app.vercel.practice.constants.VercelSharedConstants.MAIN_URL;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class BasicAuthStepDef {


    @Given("user logs in using correct credentials")
    public void user_logs_in_using_correct_credentials() {
        new BasicAuthPage().login(USERNAME, PASSWORD);
    }
    @Then("verify user has logged in by verifying text on page")
    public void verify_user_has_logged_in_by_verifying_text_on_page() {
        String footerLinkTextActual = new BasicAuthPage().footerLink.getText();
        assertThat(footerLinkTextActual).isEqualTo(EXPECTED_FOOTER_LINK_TEXT).as("BasicAuth footer text is uniquely supposed to be: " + EXPECTED_FOOTER_LINK_TEXT);
    }

    @Given("user logs in using incorrect credentials")
    public void user_logs_in_using_incorrect_credentials() {
        Faker faker = new Faker();
        new BasicAuthPage().login(faker.funnyName().name(), faker.funnyName().name());
    }
    @Then("verify user is back on home page")
    public void verify_user_is_back_on_home_page() {
        String urlActual = Driver.getDriver().getCurrentUrl();
        assertThat(urlActual).isEqualTo(MAIN_URL).as("URL when user gets auth wrong should be of main page not: " + urlActual);
    }
}
