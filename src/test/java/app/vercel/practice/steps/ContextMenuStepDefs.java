package app.vercel.practice.steps;

import app.vercel.practice.pages.ContextMenuPage;
import app.vercel.practice.utilities.BrowserUtil;
import app.vercel.practice.utilities.Driver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.Alert;
import org.openqa.selenium.interactions.Actions;

import static app.vercel.practice.constants.ContextMenuConstants.*;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class ContextMenuStepDefs {

    @Given("user right clicks on the rectangle featured on the page")
    public void user_right_clicks_on_the_rectangle_featured_on_the_page() {
        Actions actions = new Actions(Driver.getDriver());
        ContextMenuPage contextMenuPage = new ContextMenuPage();

        actions.contextClick(contextMenuPage.bigBox).perform();

    }
    @Then("user should see the javascript alert")
    public void user_should_see_the_javascript_alert() {

        //TODO: use helper method
        Alert alert = Driver.getDriver().switchTo().alert();
        assertThat(alert).isNotNull().as("The alert was not found, could not obtain its text");
        String alertTextActual = alert.getText();
        assertThat(alertTextActual).isEqualTo(ALERT_TEXT_EXPECTED)
                .as("Text of the alert window should be \"" + ALERT_TEXT_EXPECTED + "\" not: " + alertTextActual);
    }
}
