package app.vercel.practice.steps;

import app.vercel.practice.pages.RegistrationFormPage;
import app.vercel.practice.utilities.BrowserUtil;
import app.vercel.practice.utilities.DateUtil;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.datafaker.Faker;
import org.junit.jupiter.api.Assertions;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import java.time.format.DateTimeFormatter;
import java.util.Random;

import static app.vercel.practice.constants.RegistrationFormConstants.*;
public class RegistrationFormStepDef {

    private RegistrationFormPage registrationFormPage = new RegistrationFormPage();;
    private Faker faker = new Faker();
    Random random = new Random();

    /*
    -----------------------------------
    Scenario: WebOrders table is keeping track of past pizza purchases
    -----------------------------------
     */

    @Given("User enters valid first name")
    public void user_enters_valid_first_name() {
        registrationFormPage.firstNameInput.clear();
        String firstName = faker.name().firstName();
        registrationFormPage.firstNameInput.sendKeys(firstName);
        BrowserUtil.attachTextToScenario(firstName);
    }

    @Given("User enters valid last name")
    public void user_enters_valid_last_name() {
        registrationFormPage.lastNameInput.clear();
        String lastName = faker.name().lastName();
        registrationFormPage.lastNameInput.sendKeys(lastName);
        BrowserUtil.attachTextToScenario(lastName);
    }

    @Given("User enters valid username")
    public void user_enters_valid_username() {
        registrationFormPage.usernameInput.clear();
        String username = faker.funnyName().name().replace(" ", "").replace(".", "") + faker.numerify("###");
        registrationFormPage.usernameInput.sendKeys(username);
        BrowserUtil.attachTextToScenario(username);
    }

    @Given("User enters valid email address")
    public void user_enters_valid_email_address() {
        registrationFormPage.emailInput.clear();
        String email = (faker.pokemon().name() + "collector@" + faker.verb().ingForm() + faker.electricalComponents().electromechanical() + ".com").toLowerCase().replace(" ", "");
        registrationFormPage.emailInput.sendKeys(email);
        BrowserUtil.attachTextToScenario(email);
    }

    @Given("User enters valid password")
    public void user_enters_valid_password() {
        registrationFormPage.passwordInput.clear();
        String password = faker.verb().ingForm() + faker.superSmashBros().fighter();
        registrationFormPage.passwordInput.sendKeys(password);
        BrowserUtil.attachTextToScenario(password);
    }

    @Given("User enters valid phone number")
    public void user_enters_valid_phone_number() {
        registrationFormPage.phoneInput.clear();
        String phone = faker.numerify("###-###-####");
        registrationFormPage.phoneInput.sendKeys(phone);
        BrowserUtil.attachTextToScenario(phone);
    }

    @Given("User selects gender from radio buttons")
    public void user_selects_gender_from_radio_buttons() {
        int num = random.nextInt(2);
        String gender = "";
        switch (num) {
            case 0:
                registrationFormPage.maleRadioButton.click();
                gender = "Male";
                break;
            case 1:
                registrationFormPage.femaleRadioButton.click();
                gender = "Female";
                break;
            case 2:
                registrationFormPage.otherRadioButton.click();
                gender = "Other";
                break;
        }
        BrowserUtil.attachTextToScenario(gender);
    }

    @Given("User enters valid date of birth")
    public void user_enters_valid_date_of_birth() {
        registrationFormPage.birthdayInput.clear();
        String date = DateUtil.getRandomDate().format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));
        registrationFormPage.birthdayInput.sendKeys(date);
        BrowserUtil.attachTextToScenario(date);
    }

    @Given("User selects department of employment from dropdown")
    public void user_selects_department_of_employment_from_dropdown() {
        int num = random.nextInt(registrationFormPage.department.getOptions().size() - 2) + 1;
        registrationFormPage.department.selectByIndex(num);
        BrowserUtil.attachTextToScenario(registrationFormPage.department.getFirstSelectedOption().getText());
    }

    @Given("User selects job title from dropdown")
    public void user_selects_job_title_from_dropdown() {
        int num = random.nextInt(registrationFormPage.jobTitle.getOptions().size() - 2) + 1;
        registrationFormPage.jobTitle.selectByIndex(num);
        BrowserUtil.attachTextToScenario(registrationFormPage.jobTitle.getFirstSelectedOption().getText());
    }

    @Given("User selects checkbox for Java if applicable")
    public void user_selects_checkbox_for_java_if_applicable() {
        registrationFormPage.checkboxJava.click();
        BrowserUtil.attachTextToScenario("Java checkbox selected: " + registrationFormPage.checkboxJava.isSelected());
    }

    @Given("user hits Sign Up button")
    public void user_hits_sign_up_button() {
        registrationFormPage.signUpButton.click();
    }

    @Then("user should see success message for the registration form")
    public void user_should_see_success_message_for_the_registration_form() {
        String messageTextActual = registrationFormPage.successMessage.getText();
        assertThat(messageTextActual).isEqualTo(ALERT_SUCCESS_MESSAGE_EXPECTED)
                .as("Message of alert box when form is successful should be \"" + ALERT_SUCCESS_MESSAGE_EXPECTED + "\" not: " + messageTextActual);
        BrowserUtil.attachScreenShotToScenario();
    }

    /*
    -----------------------------------
    Scenario: Fill in the registration form incorrectly
    -----------------------------------
     */

    @Given("User enters a single character in the first name input field")
    public void user_enters_a_single_character_in_the_first_name_input_field() {
        registrationFormPage.firstNameInput.clear();
        String text = "A";
        registrationFormPage.firstNameInput.sendKeys(text);
        BrowserUtil.attachTextToScenario(text);
    }

    @Given("User enters a second character in the first name input field")
    public void user_enters_a_second_character_in_the_first_name_input_field() {
        String text = "A";
        registrationFormPage.firstNameInput.sendKeys(text);
        BrowserUtil.attachTextToScenario(text);
    }

    @Then("user should see error message for the registration form")
    public void user_should_see_error_message_for_the_registration_form() {
        String messageTextActual = registrationFormPage.successMessage.getText();
        assertThat(messageTextActual).isEqualTo(ALERT_FAIL_MESSAGE_EXPECTED)
                .as("Message of alert box when form is successful should be \"" + ALERT_FAIL_MESSAGE_EXPECTED + "\" not: " + messageTextActual);
        BrowserUtil.attachScreenShotToScenario();
    }

}
