package app.vercel.practice.steps;

import app.vercel.practice.pages.AutocompletePage;
import app.vercel.practice.utilities.BrowserUtil;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import java.util.*;

import static app.vercel.practice.constants.AutoCompleteConstants.AUTOCOMPLETE_OPTIONS_MULTIPLE;
import static app.vercel.practice.constants.AutoCompleteConstants.AUTOCOMPLETE_OPTIONS_SINGLE;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class AutoCompleteStepDefs {

    AutocompletePage autocompletePage = new AutocompletePage();

    @Given("verify the log contains this <country> when user enters <partial text> and presses enter")
    public void verify_the_log_contains_this_country_when_user_enters_partial_text_and_presses_enter(List<Map<String, String>> maps) {
        for (Map<String, String> map : maps){
            String userInput = map.get("partial text");
            autocompletePage.input.sendKeys(userInput + Keys.ARROW_DOWN + Keys.ENTER);
            autocompletePage.submitButton.click();
            String countryExpected = map.get("country");
            String countryActual = autocompletePage.log.getText().substring(autocompletePage.log.getText().lastIndexOf(" ") + 1);
            assertThat(countryActual).isEqualTo(countryExpected)
                    .as("Country that autocomplete finds with partial text \"" + userInput + "\" should be \"" + countryExpected + "\" not: " + countryActual);

        }

    }
    @Then("verify that autocompletion finds these countries for corresponding partial user text")
    public void verify_that_autocompletion_finds_these_countries_for_corresponding_partial_user_text(List<Map<String, String>> maps) {
        for (Map<String, String> map : maps){
            String userInput = map.get("partial text");
            autocompletePage.input.clear();
            autocompletePage.input.sendKeys(userInput);
            List<String> autocompleteActual = autocompletePage.getAutoCompleteList().stream().map(WebElement::getText).toList();
            System.out.println("<" + map.get("partial text") + " , " + map.get("country") + ">");
            String country = map.get("country");
            List<String> autocompleteExpected = new ArrayList<>();
            if(country != null){
                autocompleteExpected = Arrays.stream(map.get("country").split(", ")).toList();
            }
            assertThat(autocompleteActual).isEqualTo(autocompleteExpected);

        }
    }
}
