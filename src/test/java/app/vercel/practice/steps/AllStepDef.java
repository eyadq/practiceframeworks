package app.vercel.practice.steps;

import app.vercel.practice.constants.BasicAuthConstants;
import app.vercel.practice.pages.BasePage;
import app.vercel.practice.pages.BasicAuthPage;
import app.vercel.practice.pages.HomePage;
import app.vercel.practice.utilities.BrowserUtil;
import app.vercel.practice.utilities.ConfigurationReader;
import app.vercel.practice.utilities.Driver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;

import java.lang.reflect.InvocationTargetException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class AllStepDef {
    private static final Logger LOG = LogManager.getLogger();

    @Given("user is on {string} page")
    public void user_is_on_page(String string) {
        Driver.getDriver().get(ConfigurationReader.getProperty("practice.url"));
        LOG.info("Navigated to: " + Driver.getDriver().getCurrentUrl());
        new HomePage().goToPage(string);

    }

    @Then("verify text of {string} page is correct")
    public void verify_text_of_page_is_correct(String exampleName) {
        try {
            BasePage basePage = (BasePage) Class.forName("app.vercel.practice.pages." + exampleName.replaceAll("[^A-Za-z0-9]","") + "Page").newInstance();

            if(exampleName.equals("Basic Auth")){
                new BasicAuthPage().login(BasicAuthConstants.USERNAME, BasicAuthConstants.PASSWORD);
            }


            assertThat(basePage.getHeaderTextActual()).isEqualTo(basePage.getHeaderTextExpected());
            assertThat(basePage.getParagraphTextExpected().toString()).isEqualTo(basePage.getParagraphTextExpected().toString());

            System.out.println("Header text expected: " + basePage.getHeaderTextExpected());
            System.out.println("Header text actual:   " + basePage.getHeaderTextActual());
            System.out.println("Paragraph text expected: " + basePage.getParagraphTextExpected());
            System.out.println("Paragraph text actual:   " + basePage.getParagraphTextActual());

            basePage.getHeaderTextExpected();
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            LOG.error("The class was not found when verifying text", e);
        }
    }
}
