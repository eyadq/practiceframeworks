package app.vercel.practice.constants;

public class WebTablesConstants {

    public static final String NAME_OF_EXAMPLE = "Web Tables";
    public static final String HEADER_TEXT_EXPECTED = "List of All Pizza Orders";
    public static final String[] PARAGRAPH_TEXT_EXPECTED = {};
    public static final String GET_ALL_DATA = "SELECT FirstName || ' ' || LastName as \"Name\", PizzaType as \"Pizza Type\", Amount as \"Amount\", TO_CHAR(PurchaseDate, 'MM/DD/YYYY') as \"Date\", Street as \"Street\", City as \"City\", State as \"State\", Zip as \"Zip\", Card as \"Card\", CardNumber as \"Card Number\", Exp as \"Exp\" FROM WebOrder\n";

}
