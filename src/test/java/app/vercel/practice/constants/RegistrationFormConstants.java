package app.vercel.practice.constants;

public class RegistrationFormConstants {

    public static final String NAME_OF_EXAMPLE = "Registration Form";
    public static final String HEADER_TEXT_EXPECTED = "Registration form";
    public static final String[] PARAGRAPH_TEXT_EXPECTED = {};
    public static final String ALERT_SUCCESS_MESSAGE_EXPECTED = "Thanks for signing up!";
    public static final String ALERT_FAIL_MESSAGE_EXPECTED = "Please fix the errors below";
}
