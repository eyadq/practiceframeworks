package app.vercel.practice.constants;

public class CheckBoxesConstants {
     public static final String NAME_OF_EXAMPLE = "Checkboxes";
     public static final String HEADER_TEXT_EXPECTED = "Checkboxes";
    public static final String[] PARAGRAPH_TEXT_EXPECTED = {};
    public static final String[] CHECKBOX_LABELS = {"Checkbox 1", "Checkbox 2"};
    public static final String[] CHECKBOX_OPTIONS = {"checkbox1", "checkbox2"};
}
