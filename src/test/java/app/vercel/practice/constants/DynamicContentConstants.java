package app.vercel.practice.constants;

public class DynamicContentConstants {
    public static final String pageURL = ("https://loopcamp.vercel.app/dynamic-content.html");
    public static final String HEADER_TEXT = "Dynamic Content";
    public static final String PARAGRAPH_ABOVE_TEXT = "This example demonstrates the ever-evolving nature of content by loading new text and images on each page refresh.";
    public static final String PARAGRAPH_BELOW_TEXT = "To make some of the content static append ?with_content=static or click here.";
    public static final String IMAGE_SOURCE = "https://picsum.photos/200/300?random=";
}
