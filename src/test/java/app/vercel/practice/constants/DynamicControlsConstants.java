package app.vercel.practice.constants;

public class DynamicControlsConstants {
    public static final String pageURL = "https://loopcamp.vercel.app/dynamic-controls.html";
    public static final String HEADER_TEXT = "Dynamic Controls";
    public static final String PARAGRAPH_TEXT = "This example demonstrates when elements (e.g., checkbox, input field, etc.) are changed asynchronously.";
    public static final String HEADER_REMOVE_ADD = "Remove/add";
    public static final String CHECKBOX_MESSAGE_ENABLED = "It's back!";
    public static final String CHECKBOX_MESSAGE_DISABLED = "It's gone!";
    public static final String HEADER_ENABLE_DISABLE = "Enable/disable";
    public static final String INPUT_MESSAGE_ENABLED = "It's enabled!";
    public static final String INPUT_MESSAGE_DISABLED = "It's disabled!";
}
