package app.vercel.practice.constants;

public class BasicAuthConstants {
    public static final String NAME_OF_EXAMPLE = "Basic Auth";
    public static final String HEADER_TEXT_EXPECTED = "Basic Auth";
    public static final String[] PARAGRAPH_TEXT_EXPECTED = {"Congratulations! You must have the proper credentials."};
    public static final String USERNAME = "admin";
    public static final String PASSWORD = "admin";
    public static final String EXPECTED_FOOTER_LINK_TEXT = "CYDEO";
}
