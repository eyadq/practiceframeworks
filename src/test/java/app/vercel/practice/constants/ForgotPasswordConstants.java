package app.vercel.practice.constants;

public class ForgotPasswordConstants {
    public static final String NAME_OF_EXAMPLE = "Forgot Password";
    public static final String HEADER_TEXT_EXPECTED = "Forgot Password";
    public static final String[] PARAGRAPH_TEXT_EXPECTED = {"E-mail"};
    public static final String RETRIEVE_PASSWORD_BUTTON_TEXT = "Retrieve password";
    public static final String ERROR_MESSAGE = "Invalid email";
    public static final String EMAIL_VALID = "jdoe@gmail.com";
    public static final String EMAIL_INVALID = "jdoegmail.com";
}
