package app.vercel.practice.constants;

import app.vercel.practice.pages.BasePage;

import java.util.Map;

public class DisappearingElementsConstants {

    public static final String NAME_OF_EXAMPLE = "Disappearing Elements";
    public static final String HEADER_TEXT_EXPECTED = "Disappearing Elements";
    public static final String[] PARAGRAPH_TEXT_EXPECTED = {"This example demonstrates when elements on a page change by disappearing/reappearing on each page load."};
    public static final String[] ALL_POTENTIAL_BUTTONS = {"Home", "About", "Contact Us", "Portfolio", "Gallery"};
    public static final String HOME = "https://loopcamp.vercel.app/index.html";
    public static final String ABOUT = "https://loopcamp.vercel.app/about/index.html";
    public static final String CONTACT_US = "https://loopcamp.vercel.app/contact-us/index.html";
    public static final String PORTFOLIO = "https://loopcamp.vercel.app/portfolio/index.html";
    public static final String GALLERY = "https://loopcamp.vercel.app/gallery/index.html";
}
