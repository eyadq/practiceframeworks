package app.vercel.practice.constants;

public class EntryAdConstants {

    public static final String pageURL = "https://loopcamp.vercel.app/entry-ad.html";
    public static final String HEADER_TEXT = "Entry Ad";
    public static final String[] PARAGRAPHS = {
            "Displays an ad on page load.",
            "If closed, it will not appear on subsequent page loads.",
            "To re-enable it, click here."
    };

}
