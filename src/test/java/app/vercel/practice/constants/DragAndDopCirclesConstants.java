package app.vercel.practice.constants;

public class DragAndDopCirclesConstants {
    public static final String NAME_OF_EXAMPLE = "Drag and Drop Circles";
    public static final String HEADER_TEXT_EXPECTED = "";
    public static final String[] PARAGRAPH_TEXT_EXPECTED = {};
    public static final String BEFORE_ACTION = "Drag the small circle here.";
    public static final String DURING_ACTION_OFF_TARGET = "Drop here.";
    public static final String DURING_ACTION_ON_TARGET = "Now drop...";
    public static final String AFTER_ACTION = "You did great!";
    public static final String WRONG_ACTION = "Try again!";
    public static final String CLASS_PAINTED_BLUE = "k-header painted";
    public static final String CLASS_PAINTED_WHITE = "k-header";
}
