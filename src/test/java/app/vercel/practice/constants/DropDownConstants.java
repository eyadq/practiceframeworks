package app.vercel.practice.constants;

public class DropDownConstants {
    public static final String NAME_OF_EXAMPLE = "Dropdown";
    public static final String HEADER_TEXT_EXPECTED = "Dropdown List";
    public static final String[] PARAGRAPH_TEXT_EXPECTED = {
            "Simple dropdown",
            "Select your date of birth",
            "State selection",
            "Which programming languages do you know?",
            "Select a website"
    };
    public static final String[] SIMPLE_OPTIONS = {
            "Please select an option",
            "Option 1",
            "Option 2"
    };
    public static final String[] LANGUAGES_OPTIONS = {
            "Java",
            "JavaScript",
            "C#",
            "Python",
            "Ruby",
            "C"
    };
    public static final String[] LANGUAGES_VALUES = {
            "java",
            "js",
            "c#",
            "python",
            "ruby",
            "c"
    };
    public static final String STATE_DEFAULT_VALUE = "";
    public static final String STATE_DEFAULT_TEXT = "Select a State";
}
