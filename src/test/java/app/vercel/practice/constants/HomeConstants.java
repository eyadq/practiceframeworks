package app.vercel.practice.constants;

import java.util.Map;

public class HomeConstants {

    public static final String pageURL = "https://loopcamp.vercel.app/index.html";
    public static final String IMAGE_URL_LOGO_LOOP = "https://loopcamp.vercel.app/img/logo.svg";
    public static final String IMAGE_URL_LOGO_ACADEMY = "https://loopcamp.vercel.app/img/logo-text.svg";

    public static final String MAIN_FOOTER_POWERED_BY_LOOPCAMP = "Powered by LOOPCAMP";
    public static final String MAIN_FOOTER_LINK = "https://www.loopcamp.io/";
    public static final String HEADER_TEST_AUTOMATION_PRACTICE = "Test Automation Practice";
    public static final String HEADER_AVAILABLE_EXAMPLES = "Available Examples";
    public static final String LIST_OF_EXAMPLES =
            "A/B Testing\n" +
                    "Add/Remove Elements\n" +
                    "Autocomplete\n" +
                    "Basic Auth (user and pass: admin)\n" +
                    "Broken Images\n" +
                    "Challenging DOM\n" +
                    "Checkboxes\n" +
                    "Context Menu\n" +
                    "Disappearing Elements\n" +
                    "Drag and Drop\n" +
                    "Drag and Drop Circles\n" +
                    "Dropdown\n" +
                    "Dynamic Content\n" +
                    "Dynamic Controls\n" +
                    "Dynamic Loading\n" +
                    "Entry Ad\n" +
                    "Exit Intent\n" +
                    "File Download\n" +
                    "File Upload\n" +
                    "Floating Menu\n" +
                    "Forgot Password\n" +
                    "Form Authentication\n" +
                    "Frames\n" +
                    "Geolocation\n" +
                    "Horizontal Slider\n" +
                    "Hovers\n" +
                    "Infinite Scroll\n" +
                    "Inputs\n" +
                    "JQuery UI Menus\n" +
                    "JavaScript Alerts\n" +
                    "JavaScript onload event error\n" +
                    "Key Presses\n" +
                    "Large & Deep DOM\n" +
                    "Multiple Buttons\n" +
                    "Multiple Windows\n" +
                    "Nested Frames\n" +
                    "New tab\n" +
                    "Notification Messages\n" +
                    "Radio Buttons\n" +
                    "Redirect Link\n" +
                    "Registration Form\n" +
                    "Secure File Download\n" +
                    "Shifting Content\n" +
                    "Sign Up For Mailing List\n" +
                    "Slow Resources\n" +
                    "Sortable Data Tables\n" +
                    "Status Codes\n" +
                    "Typos\n" +
                    "WYSIWYG Editor\n" +
                    "Web Tables";
    public static final String HOME_LINKS =
            "\n\tA/B Testing has link of https://loopcamp.vercel.app/ab-test.html\n" +
                    "\tAdd/Remove Elements has link of https://loopcamp.vercel.app/add-remove-elements.html\n" +
                    "\tAutocomplete has link of https://loopcamp.vercel.app/autocomplete.html\n" +
                    "\tBasic Auth (user and pass: admin) has link of https://loopcamp.vercel.app/basic-auth.html\n" +
                    "\tBroken Images has link of https://loopcamp.vercel.app/broken-images.html\n" +
                    "\tChallenging DOM has link of https://loopcamp.vercel.app/challenging-dom.html\n" +
                    "\tCheckboxes has link of https://loopcamp.vercel.app/checkboxes.html\n" +
                    "\tContext Menu has link of https://loopcamp.vercel.app/context-menu.html\n" +
                    "\tDisappearing Elements has link of https://loopcamp.vercel.app/disappearing-elements.html\n" +
                    "\tDrag and Drop has link of https://loopcamp.vercel.app/drag-and-drop.html\n" +
                    "\tDrag and Drop Circles has link of https://loopcamp.vercel.app/drag-and-drop-circles.html\n" +
                    "\tDropdown has link of https://loopcamp.vercel.app/dropdown.html\n" +
                    "\tDynamic Content has link of https://loopcamp.vercel.app/dynamic-content.html\n" +
                    "\tDynamic Controls has link of https://loopcamp.vercel.app/dynamic-controls.html\n" +
                    "\tDynamic Loading has link of https://loopcamp.vercel.app/dynamic_loading.html\n" +
                    "\tEntry Ad has link of https://loopcamp.vercel.app/entry-ad.html\n" +
                    "\tExit Intent has link of https://loopcamp.vercel.app/exit-intent.html\n" +
                    "\tFile Download has link of https://loopcamp.vercel.app/download.html\n" +
                    "\tFile Upload has link of https://loopcamp.vercel.app/upload.html\n" +
                    "\tFloating Menu has link of https://loopcamp.vercel.app/floating-menu.html\n" +
                    "\tForgot Password has link of https://loopcamp.vercel.app/forgot-password.html\n" +
                    "\tForm Authentication has link of https://loopcamp.vercel.app/login.html\n" +
                    "\tFrames has link of https://loopcamp.vercel.app/frames.html\n" +
                    "\tGeolocation has link of https://loopcamp.vercel.app/geolocation.html\n" +
                    "\tHorizontal Slider has link of https://loopcamp.vercel.app/horizontal-slider.html\n" +
                    "\tHovers has link of https://loopcamp.vercel.app/hovers.html\n" +
                    "\tInfinite Scroll has link of https://loopcamp.vercel.app/infinite_scroll/index.html\n" +
                    "\tInputs has link of https://loopcamp.vercel.app/inputs.html\n" +
                    "\tJQuery UI Menus has link of https://loopcamp.vercel.app/jqueryui/menu.html\n" +
                    "\tJavaScript Alerts has link of https://loopcamp.vercel.app/javascript-alerts.html\n" +
                    "\tJavaScript onload event error has link of https://loopcamp.vercel.app/javascript-error.html\n" +
                    "\tKey Presses has link of https://loopcamp.vercel.app/key-presses.html\n" +
                    "\tLarge & Deep DOM has link of https://loopcamp.vercel.app/large.html\n" +
                    "\tMultiple Buttons has link of https://loopcamp.vercel.app/multiple-buttons.html\n" +
                    "\tMultiple Windows has link of https://loopcamp.vercel.app/windows.html\n" +
                    "\tNested Frames has link of https://loopcamp.vercel.app/nested-frames.html\n" +
                    "\tNew tab has link of https://loopcamp.vercel.app/open-new-tab.html\n" +
                    "\tNotification Messages has link of https://loopcamp.vercel.app/notification-message-rendered.html\n" +
                    "\tRadio Buttons has link of https://loopcamp.vercel.app/radio-buttons.html\n" +
                    "\tRedirect Link has link of https://loopcamp.vercel.app/redirector.html\n" +
                    "\tRegistration Form has link of https://loopcamp.vercel.app/registration_form.html\n" +
                    "\tSecure File Download has link of https://loopcamp.vercel.app/download_secure.html\n" +
                    "\tShifting Content has link of https://loopcamp.vercel.app/shifting_content.html\n" +
                    "\tSign Up For Mailing List has link of https://loopcamp.vercel.app/sign_up.html\n" +
                    "\tSlow Resources has link of https://loopcamp.vercel.app/slow.html\n" +
                    "\tSortable Data Tables has link of https://loopcamp.vercel.app/tables.html\n" +
                    "\tStatus Codes has link of https://loopcamp.vercel.app/status_codes.html\n" +
                    "\tTypos has link of https://loopcamp.vercel.app/typos.html\n" +
                    "\tWYSIWYG Editor has link of https://loopcamp.vercel.app/tinymce.html\n" +
                    "\tWeb Tables has link of https://loopcamp.vercel.app/web-tables.html";
}
