package app.vercel.practice.constants;

public class AddRemoveElementsConstants {
    public static final String NAME_OF_EXAMPLE = "Add/Remove Elements";
    public static final String HEADER_TEXT_EXPECTED = "Add/Remove Elements";
    public static final String[] PARAGRAPH_TEXT_EXPECTED = {};
}
