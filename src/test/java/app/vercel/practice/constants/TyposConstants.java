package app.vercel.practice.constants;

public class TyposConstants {

    public static final String NAME_OF_EXAMPLE = "Typos";
    public static final String HEADER_TEXT_EXPECTED = "Typos";
    public static final String[] PARAGRAPH_TEXT_EXPECTED = {
            "This example demonstrates a typo being introduced. It does it randomly on each page load.",
            "Sometimes you'll see a typo, other times you won't."
    };
}
