package app.vercel.practice.constants;

public class DragAndDropConstants {
    public static final String NAME_OF_EXAMPLE = "Drag and Drop";
    public static final String HEADER_TEXT_EXPECTED = "Drag and Drop";
    public static final String[] PARAGRAPH_TEXT_EXPECTED = {};
    public static final String BOX_A_TEXT = "A";
    public static final String BOX_B_TEXT = "B";
}
