package app.vercel.practice.constants;

public class ContextMenuConstants {
    public static final String NAME_OF_EXAMPLE = "Context Menu";
    public static final String HEADER_TEXT_EXPECTED = "Context Menu";
    public static final String[] PARAGRAPH_TEXT_EXPECTED = {
            "Context menu items are custom additions that appear in the right-click menu.",
            "Right-click in the box below to see one called 'the-internet'. When you click it, it will trigger a JavaScript alert."
    };
    public static final String ALERT_TEXT_EXPECTED= "You selected a context menu";
}
