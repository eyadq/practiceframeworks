package app.vercel.practice.constants;

public class VercelSharedConstants {

    public static final String MAIN_BINARY_RESOURCE_DIR = "resources/";
    public static final String MAIN_URL = "https://loopcamp.vercel.app/index.html";
}
