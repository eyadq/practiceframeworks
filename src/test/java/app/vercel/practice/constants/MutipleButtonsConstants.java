package app.vercel.practice.constants;

public class MutipleButtonsConstants {

    public static final String NAME_OF_EXAMPLE = "Multiple Buttons";
    public static final String HEADER_TEXT_EXPECTED = "Multiple buttons";
    public static final String RESULT_TEXT_EXPECTED = "Result:";
    public static final String[] PARAGRAPH_TEXT_EXPECTED = {"Here are some examples of different buttons with different attributes:"};
}
