package app.vercel.practice.constants;

public class AutoCompleteConstants {
    public static final String NAME_OF_EXAMPLE = "Autocomplete";
    public static final String HEADER_TEXT_EXPECTED = "Autocomplete";
    public static final String[] PARAGRAPH_TEXT_EXPECTED = {"Start typing:"};
    public static final String PLACEHOLDER_TEXT = "Country";
    public static final String[] AUTOCOMPLETE_OPTIONS_SINGLE = {"Yemen"};
    public static final String[] AUTOCOMPLETE_OPTIONS_MULTIPLE = {"United Arab Emirates", "United Kingdom", "United States of America"};
    public static final String LOGGED_RESULT_PALESTINE_EXPECTED = "You selected: Palestine";
    public static final String LOGGED_RESULT_ITALY_EXPECTED = "You selected: Italy";
}
