package app.vercel.practice.constants;

public class ABTestingConstants {

    public static final String NAME_OF_EXAMPLE = "A/B Testing";
    public static final String HEADER_TEXT_EXPECTED = "No A/B Test";
    public static final String[] PARAGRAPH_TEXT_EXPECTED = {"Also known as split testing. This is a way in which businesses are able to simultaneously test and learn from different versions of a page to see which text and/or functionality works best towards a desired outcome (e.g. a user action such as a click-through)."};
}
